import React, { Component } from 'react';
import FilaProducto from '../FilaProducto/FilaProducto';

class ListaProductos extends Component {

  render() {
    return (
      <div className="container-fluid">
        <ul className="collection with-header">
        <li className="collection-header"><h4>Productos</h4></li>
          {
            this.props.productos.map(function(producto, index) {
              return <FilaProducto key={index} nombre={producto.nombre} cantidad={producto.cantidad}/>;
            })
          }
        </ul>
      </div>
    )
  }
}

export default ListaProductos
