import React, { Component } from 'react';

class FilaProducto extends Component {

  render() {
    return(
      <li className="collection-item">
          {this.props.nombre}
          <div className="secondary-content">
            {this.props.cantidad}
          </div>
      </li>
    )
  }
}

export default FilaProducto
