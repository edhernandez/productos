import React, { Component } from 'react';
import ListaProducto from './ListaProductos/ListaProductos';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = { productos: [] }
  }
  
  componentWillMount() {
    fetch('http://localhost:8080/productos')
      .then((response) => {
        return response.json();
      })
      .then((productos) => {
          this.setState({ productos: productos });
      });
  }

  render() {
    return (
      <div className="App">
      <nav>
        <div className="nav-wrapper center cyan darken-1">
            Frontend del Api de Productos
        </div>
      </nav>
          <div className="container">
            {<ListaProducto productos={this.state.productos}/>}
          </div>
      </div>
    );
  }
}

export default App;
