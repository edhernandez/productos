# API y Front para productos #

## productApi ##
Contiene el api rest del proyecto

Para correrlo, necesita un mongodb corriendo en el puerto 27017. Luego correr
```
npm install
npm start
```

Para correr los test, se necesita tambien un mongodb corriendo en el mismo puerto y correr
```
npm install
npm test
```

## product-front ##
Contiene el front realizado en React

Para correrlo se necesita de productApi corriendo y ejecutar
```
npm install
npm start
```

## FizzBuzz.js ##
Contiene el ejercicio 1

## Preguntas ##

###¿De qué forma guardarías los archivos que un usuario suba por la aplicación al servidor y porque?###
Si un usuario subiese un archivo al servidor por medio de la aplicación la almacenaría en el filesystem. El primer argumento de esta elección es el trivial. El filesystem fue diseñado para guardar archivos. Indagando un poco más dentro de este motivo, si el almacenamiento se realizara en una base de datos, aunque se ganaría consistencia y, en caso de ser relacional, acciones de rollback en caso de fallas, se perdería performance, ya que al hacer un select sobre la tabla o colección estaríamos trayendo el archivo a memoria. Otro motivo nacido de lo primero dicho es que las acciones para guardar y cargar archivos son mucho mas simples utilizando el filesystem. Con una db deberíamos convertir a BLOB el archivo. Otra ventaja es que la migración hacia un nuevo servidor es sumamente simple en comparación (los dump de una base no exportan los blobs que almacenan, haciendo necesario un proceso externo que realice la descarga del viejo servidor y la carga en el nuevo).

###¿Implementaría un cache del lado del cliente? ¿Porque?###
A menos que se le indique, el cliente (navegador) ya realizará el cacheo de algunos archivos (como por ejemplo recursos de Javascript y CSS). Una caché del lado del cliente disminuye la carga del servidor al servir a los clientes y mejora la performance. Además podría permitir a una aplicación web funcionar incluso si el cliente está desconectado de la red(pensando en el estándar progressive web apps de Google)


###¿Cuál es la diferencia entre SOAP y REST?###
La principal diferencia entre SOAP Y Rest es conceptual. Rest es un patrón de arquitectura, que plantea una serie de verbos simples. Puede ser usada para un servicio web por http utilizando los verbos http (GET, POST, PUT, DELETE y PATCH). Pero el patron REST no se limita solamente al protocolo HTTP, y puede implementarse una arquitectura REST fuera de este. Por otro lado SOAP es un protocolo de mensajería basado en XML, que puede ser utilizado bajo varios protocolos, incluyendo HTTP. Aunque los servicios Rest soportan peticiones con xml, en el último tiempo se hizo común el uso de JSON.
SOAP define un contrato con las operaciones (y los tipos de datos que involucra) que los clientes deberán seguir para poder consumirlo.
REST por su parte define ciertas reglas a seguir a la hora de construir un api ( y se establecen niveles a partir de las mismas, indicando así que tan RESTful es el servicio). Una de estas reglas es la de recursos pequeños con links hacia los recursos que tenga embebidos (HATEOAS). Otra regla es la de que tanto el cliente como el servidor deben poder crecer de manera independiente. Además se define que un servicio no debe guardar ningún estado

